(put 'customize-variable 'disabled nil)

(setq org-roam-directory (file-truename "~/org/org-roam"))
(setq exec-path (append exec-path '("~/go/bin")))
(setq exec-path (append exec-path '("/usr/local/go/bin")))
(setenv "PATH" (concat (getenv "PATH") "/usr/local/go/bin"))

(setq! lsp-pylsp-plugins-black-enabled t
       lsp-pylsp-plugins-isort-enabled t
       lsp-pylsp-plugins-rope-autoimport-enabled t
       lsp-pylsp-plugins-rope-completion-enabled t

       )
