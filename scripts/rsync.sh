#!/bin/bash

echo "Sync Oh My Bash!"
mkdir -p oh-my-bash
rsync -av $HOME/.oh-my-bash oh-my-bash

echo "Sync Doom Emacs!"
mkdir -p emacs
rsync -av $HOME/.config/doom emacs

echo "Sync i3!"
mkdir -p i3
rsync -av $HOME/.config/i3 i3

echo "Sync Bash!"
mkdir -p screenlayout
rsync -av $HOME/.screenlayout screenlayout

echo "Sync Bash!"
mkdir -p git 
rsync -av $HOME/.gitconfig git/

echo "Sync Bash!"
mkdir -p bash
rsync -av $HOME/.bashrc bash/
rsync -av $HOME/.bash_profile bash/
rsync -av $HOME/.profile bash/

