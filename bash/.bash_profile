#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

PATH=$PATH:$HOME/.emacs.d/bin:$HOME/.local/bin
export PATH
